<?php
class UnionServiceQueryOrderListRequest
{
    private $orderReq = "orderReq";

    private $apiParas = array();

    public function getApiMethodName()
    {
        return "jd.union.open.order.query";
    }

    public function getApiParas()
    {
        return json_encode($this->apiParas);
    }

    public function check()
    {

    }

    public function putOtherTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }

//    private $unionId;
//
//    public function setUnionId($unionId)
//    {
//        $this->unionId = $unionId;
//        $this->apiParas["unionId"] = $unionId;
//    }
//
//    public function getUnionId()
//    {
//        return $this->unionId;
//    }

    private $time;

    public function setTime($time)
    {
        $this->time = $time;
        $this->apiParas[$this->orderReq]["time"] = $time;
    }

    public function getTime()
    {
        return $this->time;
    }

    private $pageIndex;

    public function setPageIndex($pageIndex)
    {
        $this->pageIndex = $pageIndex;
        $this->apiParas[$this->orderReq]["pageNo"] = $pageIndex;
    }

    public function getPageIndex()
    {
        return $this->pageIndex;
    }

    private $pageSize;

    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;
        $this->apiParas[$this->orderReq]["pageSize"] = $pageSize;
    }

    public function getPageSize()
    {
        return $this->pageSize;
    }

    private $type;

    public function setType($type)
    {
        $this->type = $type;
        $this->apiParas[$this->orderReq]["type"] = $type;
    }

    public function getType()
    {
        return $this->type;
    }

}
