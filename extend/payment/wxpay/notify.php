<?php
ini_set('date.timezone','Asia/Shanghai');
error_reporting(E_ERROR);

require_once "lib/WxPay.Api.php";
require_once 'lib/WxPay.Notify.php';
require_once 'log.php';

//初始化日志
$logHandler= new CLogFileHandler(env('extend_path')."/payment/wxpay/logs/".date('Y-m-d').'.log');
$log = Log::Init($logHandler, 15);

class PayNotifyCallBack extends WxPayNotify
{
	//查询订单
	public function Queryorder($transaction_id)
	{
		$input = new WxPayOrderQuery();
		$input->SetTransaction_id($transaction_id);
		$result = WxPayApi::orderQuery($input);
		Log::DEBUG("query:" . json_encode($result));
		if(array_key_exists("return_code", $result)
			&& array_key_exists("result_code", $result)
			&& $result["return_code"] == "SUCCESS"
			&& $result["result_code"] == "SUCCESS")
		{
			return true;
		}
		return false;
	}
	
	//重写回调处理函数
	public function NotifyProcess($data, &$msg)
	{
		Log::DEBUG("call back:" . json_encode($data));
		$notfiyOutput = array();
		
		if(!array_key_exists("transaction_id", $data)){
			$msg = "输入参数不正确";
			return false;
		}
		//查询订单，判断订单真实性
		if(!$this->Queryorder($data["transaction_id"])){
			$msg = "订单查询失败";
			return false;
		}
		//处理业务--会员升级支付的回调
		$out_trade_no = $data['out_trade_no'];
		$amount = $data['total_fee']/100;
		$log = model('Paylogs')->get(['out_trade_no'=>$out_trade_no,'amount'=>$amount,'pay_type'=>1]);
		if ($log->pay_status==0){
		    $log->pay_status = 1;
		    $log->trade_no = $data['transaction_id'];
		    if ($log->save()){
		        //购买者信息
		        $uinfo = db('User')->where('uid',$log->uid)->find();
		        $data = [
		            'leval' => 3,
		            'last_leval_time' => time(),
		        ];
		        if (db('User')->where('uid',$log->uid)->update($data)){
		            //市代自动升级
		            auto_shidai($uinfo['pid']);
		            //升级
		            $lvcfg = get_db_config(true)['user_leval_cfg'];
		            //星火
		            xinghuo($log->uid, $uinfo['pid'], 3, $uinfo['leval'], $lvcfg);
		            //如果升级成为钻石，无限上级（最近） 市级以上额外分10元,20180415 add
		            $ew_money = $lvcfg['gt3_ewjl'];
		            wx_gt3_ewjl($uinfo['pid'],$uid);
		        }
		    }
		}
		return true;
	}
}

Log::DEBUG("begin notify");
//$notify = new PayNotifyCallBack();
//$notify->Handle(false);
