<?php /*a:2:{s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/user/index.html";i:1546095382;s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/base.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php if(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty())): ?><?php echo htmlentities($site['name']); else: ?><?php echo htmlentities($title); ?>-<?php echo htmlentities($site['name']); ?><?php endif; ?></title>
	<link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css" />
	<link rel="stylesheet" type="text/css" href="/static/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/static/css/common.css" />
	
</head>

<body>
	<div class="admin-body">
		
<div class="layui-tab layui-tab-brief" style="margin: -3px 0 0 -10px;" lay-filter="tb">
  <ul class="layui-tab-title">
    <li class="layui-this">个人信息</li>
    <li>修改密码</li>
    <li>操作日志</li>
  </ul>
  <div class="layui-tab-content">
    <div class="layui-tab-item layui-show">
    	<form class="layui-form form1" action="">
		  <div class="layui-form-item">
		    <label class="layui-form-label">登录账号</label>
		    <div class="layui-form-mid layui-word-aux"><?php echo htmlentities($info['username']); ?></div>
		  </div>
		  <div class="layui-form-item">
		    <label class="layui-form-label">所属用户组</label>
		    <div class="layui-form-mid layui-word-aux"><?php echo htmlentities($mygroup['name']); ?></div>
		  </div>
		  <div class="layui-form-item">
		    <label class="layui-form-label">昵称</label>
		    <div class="layui-input-inline" style="width: 300px;">
		      <input type="text" name="nickname" value="<?php echo htmlentities($info['nickname']); ?>" required lay-verify="required" autocomplete="off" class="layui-input">
		    </div>
		  </div>
		  <div class="layui-form-item">
		    <label class="layui-form-label">来个niubility的头像</label>
		    <div class="headimg layui-inline">
		    	<?php if($info['headimg'] == '0'): ?>
		     	<img src="/static/images/0.jpg" class="layui-circle" width="100" height="100" />
		     	<?php else: ?>
		     	<img src="<?php echo htmlentities(get_img($info['headimg'])); ?>" class="layui-circle" width="100" height="100" />
		     	<?php endif; ?>
		     	<input type="hidden" name="headimg" value="<?php echo htmlentities($info['headimg']); ?>">
		    </div>
		    <button type="button" class="layui-btn layui-btn-small" id="himg">
			  <i class="layui-icon">&#xe67c;</i>上传图片
			</button>
		  </div>
		  <div class="layui-form-item">
		    <label class="layui-form-label">邮箱</label>
		    <div class="layui-input-inline" style="width: 300px;">
		      <input type="text" name="email" value="<?php echo htmlentities($info['email']); ?>" required lay-verify="email" autocomplete="off" class="layui-input">
		    </div>
		  </div>

		  
		  <div class="layui-form-item">
		    <div class="layui-input-block">
		      <button class="layui-btn" lay-submit lay-submit="" lay-filter="ajax-post" type="submit" target-form="form1">立即提交</button>
		      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
		  </div>
		</form>
    </div>
    <div class="layui-tab-item">
    	<form class="layui-form form2" action="<?php echo url('editpwd'); ?>" >
		  <div class="layui-form-item">
		    <label class="layui-form-label">登录账号</label>
		    <div class="layui-form-mid layui-word-aux"><?php echo htmlentities($info['username']); ?></div>
		  </div>
		  <div class="layui-form-item">
		    <label class="layui-form-label">旧密码</label>
		    <div class="layui-input-inline">
		      <input type="password" name="oldpassword" required lay-verify="required" placeholder="请输入密码" autocomplete="off" class="layui-input">
		    </div>
		  </div>
		  <div class="layui-form-item">
		    <label class="layui-form-label">新密码</label>
		    <div class="layui-input-inline">
		      <input type="password" name="newpassword" required lay-verify="required" placeholder="请输入密码" autocomplete="off" class="layui-input">
		    </div>
		  </div>
		  <div class="layui-form-item">
		    <label class="layui-form-label">确认新密码</label>
		    <div class="layui-input-inline">
		      <input type="password" name="renewpassword" required lay-verify="required" placeholder="请输入密码" autocomplete="off" class="layui-input">
		    </div>
		  </div>
		  <div class="layui-form-item">
		    <div class="layui-input-block">
		      <button class="layui-btn" lay-submit lay-submit="" lay-filter="ajax-post" type="submit" target-form="form2">立即提交</button>
		      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
		  </div>
		 </form>
    </div>
    <div class="layui-tab-item"><table id="table1" lay-filter="_table1"></table></div>
  </div>
</div> 


	</div>
	
</body>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript">layui.config({base: '/static/js/'});</script>

<script type="text/javascript">
layui.use(['tool','upload','element'],function(){
	var upload = layui.upload,$=layui.$,layer=layui.layer,element = layui.element,table = layui.table;
	//执行实例
	var uploadInst = upload.render({
		elem: '#himg' //绑定元素
		,url: '<?php echo url('Ajax/upload'); ?>' //上传接口
		,done: function(res){
		  //上传完毕回调
		  if (res.code==1) {
		    	$('input[name=headimg]').val(res.data.id);
		    	$('.headimg img').attr('src',res.data.url);
		    }else{
		    	layer.msg(res.data,{icon:2});
		    }
		}
		,error: function(){
		  //请求异常回调
		  layer.msg('上传异常~');
		}
	});
	element.on('tab(tb)', function(data){
	  if (data.index == 2) {
	  	var tableobj = table.render({
			elem:'#table1',
			url:'<?php echo url('mylogs'); ?>',
			limit:20,
			limits:[10,20,50,100],
			page:true,
			//size:'sm',
			method:'get',
			height:'full-90',
			cols:[[
				{title:'账号',field:'username',width:100},
				{title:'事件标题',field:'title'},
				{title:'url',field:'url'},
				{title:'参数内容',field:'content'},
				{title:'IP',field:'ip'},
				{title:'时间',field:'create_time'},
			]]
		});
	  }
	});
	

});
</script>

</html>