<?php
namespace app\api\controller;

use think\Controller;
use app\common\controller\Api;

class Index extends Api
{
    public function index()
    {
        //$db_config = get_db_config(true);
        //dump($db_config);
    }
    /**
     * 淘宝客信息
     */
    public function get_taoke_cfg()
    {
        $data = [
            'tk_pid'=>$this->tk_pid,
            'tk_siteid' => $this->tk_siteid,
            'tk_adzoneid'=>$this->tk_adzoneid,
            'tk_appkey' => $this->tk_appkey,
            'bc_pid' => $this->bc_pid,
            'bc_appkey'=>$this->bc_appkey,
        ];
        $this->result($data,1);
    }
    /**
     * 热搜词、缓存12小时 好单库获取
     */
    public function hot_words()
    {
        $words = cache('api_hot_words');
        if (!$words){
            $url = 'http://v2.api.haodanku.com/hot_key/apikey/duoduojie';
            $re = json_decode(http_get($url),true);
            $words = [];
            foreach ($re['data'] as $k=>$v){
                $words[] = $v['keyword'];
            }
            cache('api_hot_words',$words,3600*12);
        }
        $this->result($words,1);
    }
    /**
     * 关联建议词
     */
    public function suggest()
    {
        $q = input('q');
        $url = 'https://suggest.taobao.com/sug?code=utf-8&q='.$q;
        if (!empty($q)){
            $re = json_decode(http_get($url),true);
            $words = [];
            foreach ($re['result'] as $k=>$v){
                $words[] = $v[0];
            }
            $this->result($words,1);
        }else {
            $this->result('请输入关键词');
        }
    }
    /**
     * 首页各种图及布局
     */
    public function index_layout()
    {
        $dbcfg = get_db_config(true);
        $this->assign('dbcfg',$dbcfg);
        $this->assign('app_func_action',config('site.app_func_action'));
        $data = [
            'html' => $this->fetch(),
        ];
        $this->result($data,1);
    }
    /**
     * 客服
     */
    public function kf()
    {
        $db_config = get_db_config();
        $kf_cfg = json_decode($db_config['app_kf'],true);
        $a = new Article();
        $info = $a->index($kf_cfg['article_id']);
        $data = [
            'weixin' => $kf_cfg['weixin'],
            'content' => replace_con_img($info['content'])
        ];
        return $this->result($data,1);
    }
    /**
     * 搜索教程
     */
    public function sq_help()
    {
        $db_config = get_db_config();
        $sq_cfg = json_decode($db_config['app_souquan_help'],true);
        $a = new Article();
        $content1 = replace_con_img($a->index($sq_cfg[1])['content']);
        $content2 = replace_con_img($a->index($sq_cfg[2])['content']);
        $content3 = replace_con_img($a->index($sq_cfg[3])['content']);
        $data = [
            'content1' => $content1,
            'content2' => $content2,
            'content3' => $content3,
        ];
        return $this->result($data,1);
    }
    /**
     * 登录配置
     */
    public function login_cfg()
    {
        $db_config = get_db_config(true);
        $data = [
            'wx' => $db_config['app_wx_login'],
            'apple' => $db_config['app_apple_examine'],
            'need_code' => (int)$db_config['invite_need_code'],
            'user_protocol_url' => $db_config['app_user_protocol'],
        ];
        return $this->result($data,1);
    }
    
    
    
    
    
    
    
    
    
}



















