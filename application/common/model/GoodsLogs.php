<?php

namespace app\common\model;

use think\Model;

class GoodsLogs extends Model
{
    // 关闭自动写入update_time字段
    //protected $updateTime = false;
    public function getUserNicknameAttr($value,$data){
        $nickname = get_nickname($data['uid']);
        if (!$nickname){
            //$nickname = '无用户';
        }
        return $nickname;
    }
    //浏览类型
    public function getViewTypeTextAttr($value,$data)
    {
        $text = ['推荐','搜索'];
        return $text[$data['view_type']];
    }
    //浏览or收藏
    public function getIsCollectTextAttr($value,$data)
    {
        $text = ['浏览','收藏'];
        return $text[$data['is_collect']];
    }
    public function getIsTmallTextAttr($value,$data)
    {
        $text = ['淘宝','天猫','京东','拼多多'];
        return $text[$data['is_tmall']];
    }
    
}
