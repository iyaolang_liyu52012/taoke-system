<?php

namespace app\common\model;

use think\Model;

class Orders extends Model
{
    // 关闭自动写入update_time字段
    //protected $updateTime = false;
    //protected $createTime = false;
    
    public function getOrderStatusTextAttr($value,$data)
    {
        //订单状态 1订单付款 2订单结算 3订单失效 4订单成功5维权订单
        $status_text = ['',
            '<span class="layui-badge ">订单付款</span>',
            '<span class="layui-badge layui-bg-green">订单结算</span>',
            '<span class="layui-badge layui-bg-gray">订单失效</span>',
            '<span class="layui-badge layui-bg-orange">订单成功</span>',
            '<span class="layui-badge layui-bg-orange"">退款</span>'
        ];
        return $status_text[$data['order_status']];
    }
    public function getUserNicknameAttr($value,$data){
        $nickname = get_nickname($data['uid']);
        if (!$nickname){
            //$nickname = '无用户';
        }
        return $nickname;
    }
    public function getTjpTextAttr($value,$data)
    {
        $text = ['t'=>'淘宝','j'=>'京东','p'=>'拼多多'];
        return $text[$data['tjp']];
    }
    public function getBuildTimeTextAttr($value,$data)
    {
        return date('Y-m-d H:i:s',$data['build_time']);
    }
    public function getEndTimeTextAttr($value,$data)
    {
        if ($data['end_time']==0){
            return '未结算';
        }
        return date('Y-m-d H:i:s',$data['end_time']);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
