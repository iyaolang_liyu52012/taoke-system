<?php

namespace app\common\model;

use think\Model;

class Draw extends Model
{
    public function getUserNicknameAttr($value,$data){
        $nickname = get_nickname($data['uid']);
        if (!$nickname){
            //$nickname = '无用户';
        }
        return $nickname;
    }
    public function getStatusTextAttr($value,$data){
        $text = ['<span class="layui-badge layui-bg-orange">待审核</span>','<span class="layui-badge layui-bg-green">提现成功</span>','<span class="layui-badge">提现驳回</span>','<span class="layui-badge layui-bg-blue">标记成功</span>'];
        return $text[$data['status']];
    }
    public function getDrawTypeTextAttr($value,$data){
        $text = ['微信','支付宝'];
        return $text[$data['draw_type']];
    }
}
