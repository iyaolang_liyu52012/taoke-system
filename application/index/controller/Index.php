<?php
namespace app\index\controller;
use app\common\controller\Sms;
use think\Controller;
class Index extends Controller
{
    public function index()
    {
        //fenyong_leval(4, 100010, 99);
    }

    /**
     * 邀请页面
     */
    public function invite()
    {
        $db_config = get_db_config();
        $app_down_url = $db_config['app_down_url'];
        
        $code = input('code');
        /* $status = db('user')->where('invite_code',$code)->value('status');
        if ($status!=1){
            $code = '';
        } */
        $this->assign('code',$code);
        //$this->assign('invite_wap_banner',$db_config['invite_wap_banner']);
        $this->assign('app_down_url',$db_config['app_down_url']);
        return $this->fetch();
        
        
        
    }
    /**
     * 提交
     */
    public function do_invite(){
        $db_config = get_db_config();
        $app_down_url = $db_config['app_down_url'];
        if ($this->request->isPost()){
            $mobile = input('mobile');
            $vcode = input('vcode');
            if (empty($mobile)) $this->error('请输入手机号');
            if(!preg_match(config('site.regex_mobile'),$mobile)) $this->error('手机号格式不正确');
            if (empty($vcode)) $this->error('请输入短信验证码');
            //手机是否可用
            if (model('User')->get(['mobile'=>$mobile])){
                $this->result(['downurl'=>$app_down_url],2,'此手机已经注册.');
            }elseif (model('InviteLogs')->get(['mobile'=>$mobile])){
                $this->result(['downurl'=>$app_down_url],2,'此手机已经注册');
            }
            //验证码是否正确
            $smsfind = model('Sms')->where('used',0)->where('type','invite')->where('mobile',$mobile)->order('create_time desc')->find();
            if ($smsfind){
                $codearr = json_decode($smsfind['code'],true);
                if ($codearr['code']==$vcode){
                    if ( (time()-$smsfind->getData('create_time')) > (15*60) ){
                        $this->error('验证码已过期，请重新获取');
                    }
                    //入库
                    $code = input('code');
                    $pid = 0;
                    if (!empty($code)){
                        $pid = db('user')->where('invite_code',$code)->value('uid')?:0;
                    }
                    $data = [
                        'uid' => $pid,
                        'mobile' => $mobile
                    ];
                    model('InviteLogs')->save($data);
                    //使验证码失效
                    $smsfind->used = 1;
                    $smsfind->save();
                    $this->success('注册成功',$app_down_url);
                }else {
                    $this->error('验证码错误');
                }
            }else {
                $this->error('验证码错误~');
            }
        }
    }
    /**
     * 发送短信验证码
     * type:0注册登录 1：绑定手机
     */
    public function sendsms()
    {
        $mobile = input('post.mobile');
        if(!preg_match(config('site.regex_mobile'),$mobile)) {
            $this->error('手机号格式不正确');
        }
        $type = intval(input('post.type',1));
        $sms = new Sms();
        if ($type==1){
            $db_config = get_db_config();
            $app_down_url = $db_config['app_down_url'];
            if (model('User')->get(['mobile'=>$mobile])){
                $this->result(['downurl'=>$app_down_url],2,'此手机已经注册.');
            }elseif (model('InviteLogs')->get(['mobile'=>$mobile])){
                $this->result(['downurl'=>$app_down_url],2,'此手机已经注册');
            }
            $re = $sms->invite($mobile,['code'=>mt_rand(1000,9999)]);
        }else {
            $this->error('操作错误！');
        }
        //trace($re);
        if ($re['Code']=='OK'){
            $this->success('发送成功');
        }else {
            $this->error('短信发送失败，请稍候重试');
        }
    }
    
    /**
     * 导入订单
     */
    public function import_order()
    {
        $key = input('key');
        if ($key!='xx00oo113322'){
            exit('error!');
        }
        $type = input('type');
        $time = input('time','');
        switch ($type){
            case 'tb1':
                //按创建时间导入淘宝订单
                $span = 1200;
                if ($time){
                    import_order_tb($span,'create_time',$time);
                }
                import_order_tb($span);
                break;
            case 'tb2':
                //按结算时间导入淘宝订单
                $span = 1200;
                if ($time){
                    import_order_tb($span,'settle_time',$time);
                }
                import_order_tb($span,'settle_time');
                break;
            case 'jd':
                $date = date('YmdH');
                if ($time){
                    $date = $time;
                }
                import_order_jd($date);//当前这个小时
                break;
            case 'pdd':
                $span = time()-60*10;
                if ($time){
                    $span = $time;
                }
                import_order_pdd($span);//十分钟前
                break;
            case 'fanli':
                fanli();
                break;
            default:
                exit('参数无效');
        }
    }
    
    
}
