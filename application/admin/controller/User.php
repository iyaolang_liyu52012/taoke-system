<?php
// +----------------------------------------------------------------------
// | llpt system
// +----------------------------------------------------------------------
// | Copyright (c) 2017 llpt All rights reserved.
// +----------------------------------------------------------------------
// | Author: Gooe <zqscjj@163.com> QQ:81009953
// +----------------------------------------------------------------------
namespace app\admin\controller;
use app\common\controller\Admin;
/**
* 个人管理
*/
class User extends Admin
{
	protected $model;
	protected $noNeedRight = ['*'];
	public function initialize()
	{
		parent::initialize();
		$this->model = model('Admin');
	}
	
	/**
	 * 个人信息
	 */
	public function index()
	{
		if (IS_AJAX){
			$data = input('post.');
			if ($this->model->allowField(['nickname','headimg','email'])->save($data,['uid'=>$this->auth->uid]))
			{
				//因为个人资料面板读取的Session显示，修改自己资料后同时更新Session
				$admin = session('admin');
				$admin_id = $admin ? $admin->uid : 0;
				if($this->auth->uid==$admin_id){
					$admin = $this->model->get($admin_id);
					session("admin", $admin);
				}
				$this->success('更新成功');
			}
			else 
			{
				$this->error('更新失败');
			}
			return ;
		}
		$info = $this->model->get($this->auth->uid);
		$this->assign('info',$info);
		$mygroup = $this->auth->getGroups($this->auth->uid);
		$this->assign('mygroup',$mygroup[0]);
		return $this->fetch();
	}
	/**
	 * 修改密码
	 */
	public function editpwd()
	{
		if (IS_AJAX)
		{
			$info = $this->model->get($this->auth->uid);
			$oldpwd = input('post.oldpassword');
			$newpwd = input('post.newpassword');
			$renewpwd = input('post.renewpassword');
			if ($newpwd!==$renewpwd)
			{
				$this->error('两次密码输入的不一致！');
			}
			if (strlen($newpwd)<6 || strlen($newpwd)>20)
			{
				$this->error('密码长度在6-20位！');
			}
			if ($info->password != md5(md5($oldpwd) . $info->salt))
			{
				$this->error('旧密码错误~');
			}
			//开始修改密码
			$salt = \ddj\Random::alnum();
			$info->salt = $salt;
			$info->password = md5(md5($newpwd) . $salt);
			if ($info->save())
			{
				$this->success('密码修改成功');
			}
			else 
			{
				$this->error('密码修改失败');
			}
		}
	}
	/**
	 * 个人日志
	 */
	public function mylogs()
	{
	if (IS_AJAX)
    	{
    		$list = model('AdminLog')
    		->where('username',$this->auth->username)
    		->order('id desc')
    		->paginate(input('limit',15));
    		//数据转换
    		return json(layui_page($list));
    	}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
