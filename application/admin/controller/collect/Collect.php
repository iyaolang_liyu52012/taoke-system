<?php

namespace app\admin\controller\collect;

use think\Controller;
use app\common\controller\Admin;

class Collect extends Admin
{
    protected $model;
    protected $default_cid = [1,2,3,4,5,6,7,8,9,10];//默认分类
    public function initialize()
    {
        parent::initialize();
        $this->model = model('GoodsCollect');
    }
    /**
     * 首页
     * @return \think\response\Json|mixed|string
     */
    public function index()
    {
        if ($this->request->isAjax())
        {
            $map = [];
            //来源
            $from = input('from','');
            if (!empty($from)){
                $map[] = ['from','eq',$from];
            }
            //搜索
            $price_min = (int)input('price_min',0);
            $price_max = (int)input('price_max',0);
            $cid = (int)input('cid',0);
            $from = input('from','');
            $keyword = input('keyword','');
            if ($price_min>0 || $price_max>0){
                if ($price_min>0 && $price_max<=0){
                    $map[] = ['price_after_quan','>=',$price_min];
                }elseif ($price_min<=0 && $price_max>0){
                    $map[] = ['price_after_quan','<=',$price_max];
                }else {
                    if ($price_max == $price_min){
                        $map[] = ['price_after_quan','=',$price_max];
                    }else {
                        $map[] = ['price_after_quan','>=',$price_min];
                        $map[] = ['price_after_quan','<=',$price_max];
                    }
                }
            }
            if ($cid>0){
                $map[] = ['cid','=',$cid];
            }
            if (!empty($from)){
                $map[] = ['from','=',$from];
            }
            if (!empty($keyword)){
                if (is_numeric($keyword)){
                    $map[] = ['item_id','eq',$keyword];
                }else {
                    $map[] = ['title','like',"%{$keyword}%"];
                }
            }
            
            $list = $this->model
            ->where($map)
            ->order('id desc')
            ->paginate(input('limit',15));
            $list->append(['cid_text','is_tmall_text']);
            //返回layui分页
            return json(layui_page($list));
        }
        //所有版块
        $sections = db('GoodsSection')->where('status',1)->order('sort desc')->column('name','id');
        $this->assign('sections',$sections);
        return $this->fetch();
    }
    /**
     * 导入
     */
    public function import()
    {
        if (!IS_AJAX) return '';
        $goods_collect = db('goods_collect');
        $type = input('type');
        $section_id = input('section_id');
        $map = [];
        //导入所选
        if ($type=='checked'){
            $ids = input('ids');
            parse_str(html_entity_decode($ids),$ids);
            $ids = $ids['ids'];
            if (empty($ids)){
                $this->error('请选择你要导入的数据');
            }
            $map[] = ['id','in',$ids];
        }elseif ($type=='all'){//导入所有
            
        }elseif ($type=='searched'){
            $ids = input('ids');
            parse_str(html_entity_decode($ids),$ids);
            //搜索
            $price_min = (int)$ids['price_min'];
            $price_max = (int)$ids['price_max'];
            $cid = (int)$ids['cid'];
            $from = $ids['from'];
            $keyword = $ids['keyword'];
            if ($price_min>0 || $price_max>0){
                if ($price_min>0 && $price_max<=0){
                    $map[] = ['price_after_quan','>=',$price_min];
                }elseif ($price_min<=0 && $price_max>0){
                    $map[] = ['price_after_quan','<=',$price_max];
                }else {
                    if ($price_max == $price_min){
                        $map[] = ['price_after_quan','=',$price_max];
                    }else {
                        $map[] = ['price_after_quan','>=',$price_min];
                        $map[] = ['price_after_quan','<=',$price_max];
                    }
                }
            }
            if ($cid>0){
                $map[] = ['cid','=',$cid];
            }
            if (!empty($from)){
                $map[] = ['from','=',$from];
            }
            if (!empty($keyword)){
                if (is_numeric($keyword)){
                    $map[] = ['item_id','eq',$keyword];
                }else {
                    $map[] = ['title','like',"%{$keyword}%"];
                }
            }
        }
        
        $goods_collect->where($map)->chunk(100, function($goods) use ($section_id){
            _import($section_id, $goods);
        });
        $this->success('导入成功');
    }
    /**
     * 删除
     */
    public function del($ids='')
    {
        $type = input('type');
        if ($type=='all'){
            $count = db('goods_collect')->delete(true);
            if ($count){
                $this->success('删除成功');
            }else {
                $this->error('删除失败');
            }
        }else {
            if ($ids){
                $count = $this->model->where('id','in',$ids)->delete();
                if ($count){
                    $this->success('删除成功');
                }else {
                    $this->error('删除失败');
                }
            }else{
                $this->error('请选择您要操作的数据');
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
