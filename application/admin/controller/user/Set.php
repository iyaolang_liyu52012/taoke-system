<?php
// +----------------------------------------------------------------------
// | duoduojie app system
// +----------------------------------------------------------------------
// | Copyright (c) 2017 duoduojie app All rights reserved.
// +----------------------------------------------------------------------
// | Author: Gooe <zqscjj@163.com> QQ:81009953
// +----------------------------------------------------------------------
namespace app\admin\controller\user;
use app\common\controller\Admin;
class Set extends Admin
{
    public function initialize()
    {
        parent::initialize();
    }
    /**
     * 会员等级设置
     */
    public function index()
    {
        //$user_lvname = config('site.user_leval');
        if ($this->request->isPost()){
            $lvcfg = input('post.lvcfg/a');
            if (is_array($lvcfg['kanjia']['rules'])){
                $lvcfg['kanjia']['rules'] = array_values($lvcfg['kanjia']['rules']);
            }
            $lvcfg = json_encode($lvcfg);
            if (model('Config')->where('name','user_leval_cfg')->update(['value'=>$lvcfg,'update_time'=>time()])){
                //清除数据库配置缓存
                cache('db_config',null);
                $this->success('保存成功');
            }else {
                $this->error('保存失败');
            }
        }else {
            $lvcfg = $this->config['user_leval_cfg'];
            //dump($lvcfg);die;
            $this->assign('lvcfg',$lvcfg);
            return $this->fetch();
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
