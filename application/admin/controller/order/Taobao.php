<?php

namespace app\admin\controller\order;

use think\Controller;
use think\Request;
use app\common\controller\Admin;

class Taobao extends Admin
{
    protected $model = null;
    public function initialize()
    {
        parent::initialize();
        $this->model = model('Orders');
    }
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        if ($this->request->isAjax())
        {
            $map = [
                ['status','neq',-1]
            ];
            //订单状态
            $order_status = input('order_status');
            if (!empty($order_status)){
                $map[] = ['order_status','eq',$order_status];
            }
            //订单类型
            $tjp = input('tjp');
            if ($tjp){
                $map[] = ['tjp','=',$tjp];
            }
            //时间条件
            $data = input('date');
            if ($data){
            	list($date_start,$date_end) = str2arr($data,' ~ ');
            	$map[] = ['create_time','between time',[$date_start,$date_end]];
            }
            //智能识别关键词
            $keyword = input('keyword');
            if (!empty($keyword)){
                if (is_numeric($keyword) && strlen($keyword)==18){
                    $map[] = ['trade_id','eq',$keyword];
                }elseif (is_numeric($keyword) && strlen($keyword)==6){
                    $map[] = ['uid','eq',$keyword];
                }elseif (is_numeric($keyword) && strlen($keyword)<6){
                    $map[] = ['id','eq',$keyword];
                }
                else {
                    $map[] = ['title','like',"%{$keyword}%"];
                }
            }
            $list = $this->model
            ->where($map)
            ->order('create_time desc')
            ->paginate(input('limit',15));
            $list->append(['order_status_text','user_nickname','tjp_text','build_time_text','end_time_text']);
            //返回layui分页
            return json(layui_page($list));
        }
        return $this->fetch();
    }


    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function del($ids='')
    {
        parent::del($ids);
    }
    /**
     * 导出会员列表、提现记录、支付记录
     */
    public function export()
    {
    	$params = input();
    	$ids = $params['ids'] ?? [];
    	$type = empty($params['type'])?'draw':$params['type'];
    	$map = [];
    	$map[] = ['status','neq',-1];
    	//无选中
    	if (empty($ids)){
    		//日期
    		if ($params['date']){
    			list($date_start,$date_end) = str2arr($params['date'],' ~ ');
    		}else{
    			$date_start = date('Y-m-d 00:00:00',time());
    			$date_end = date('Y-m-d 23:59:59',time());
    		}
    		$map[] = ['create_time','between time',[$date_start,$date_end]];
    	}else {
    		$map[] = ['id','in',$ids];
    	}
    	//订单状态
    	$order_status = input('order_status');
    	if (!empty($order_status)){
    		$map[] = ['order_status','eq',$order_status];
    	}
    	//订单类型
    	$tjp = input('tjp');
    	if ($tjp){
    		$map[] = ['tjp','=',$tjp];
    	}
    	//智能识别关键词
    	$keyword = input('keyword');
    	if (!empty($keyword)){
    		if (is_numeric($keyword) && strlen($keyword)==18){
    			$map[] = ['trade_id','eq',$keyword];
    		}elseif (is_numeric($keyword) && strlen($keyword)==6){
    			$map[] = ['uid','eq',$keyword];
    		}elseif (is_numeric($keyword) && strlen($keyword)<6){
    			$map[] = ['id','eq',$keyword];
    		}
    		else {
    			$map[] = ['title','like',"%{$keyword}%"];
    		}
    	}
    	switch ($type){
    		case 'order':
    			$list = $this->model->where($map)->field('uid,trade_id,title,order_status,order_type,pay_price,item_num,income_rate,income_price,build_time,end_time')->select();
    			$status_arr = ['未知','订单付款','订单结算','订单失效','订单成功','维权退款'];
    			$order_type = ['淘宝','天猫','京东','拼多多'];
    			foreach ($list as $k=>$v){
    				$list[$k]['order_status'] = $status_arr[$v['order_status']];
    				$list[$k]['order_type'] = $order_type[$v['order_type']];
    				$list[$k]['build_time'] = date('Y-m-d H:i:s',$v['build_time']);
    				$list[$k]['end_time'] = date('Y-m-d H:i:s',$v['end_time']);
    			}
    			$headArr = ['用户ID','单号','商品名称','订单状态','订单类型','实际付款','数量','收入比例(%)','收入金额','付款时间','结算时间'];
    			//设置一些宽度
    			$width = [
    				'B' => 20,
    				'C' => 50,
    				'J' => 20,
    				'K' => 20,
    			];
    			set_time_limit(0);
    			ini_set("memory_limit", "2048M");
    			export_excel('订单列表', $headArr, $list->toArray(),$width);
    			break;
    	}
    }
}
